-- INSERTING records

INSERT INTO artists (name) VALUES ("Michael Pangilinan");
INSERT INTO artists (name) VALUES ("Bugoy Drilon");

-- [INSERT]For multiple values
INSERT INTO albums (albumTitle, dateReleased, artistId) VALUES ("Michael", "2016-1-1", 1);
INSERT INTO songs (songName, length, genre, albumId) VALUES ("Only You", 357, "R&B", 4);



-- READING/RETRIEVE RECORDS
-- [READ] All records
SELECT * FROM songs;

-- [READ] Specific
SELECT songName, genre FROM songs WHERE songName = "Paano na kaya?" AND genre = "R&B";

-- Display title and length of R&B songs that are more than 3 mins.
SELECT songName, length FROM songs WHERE length > 300 AND genre = "R&B";




-- UPDATING RECORDS
UPDATE songs SET length = 259 WHERE songName = "Bakit Ba?";


-- DELETING RECORDS
DELETE FROM songs WHERE genre = "R&B" AND length < 300;